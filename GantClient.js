////////////////////////////////////// Function called in the Html file
$(function() {
  const socket = io();

  ////////////////////////////////////// Initialising Gantt from the library "Dhtmlx Gantt"
  gantt.init("gantt");

  ////////////////////////////////////// Loading the Get route to initialize them in the gantt
  gantt.load("/load");

  ////////////////////////////////////// Saving the changes
  let dp = new gantt.dataProcessor("/data");
  dp.init(gantt);
  dp.setTransactionMode("REST");

  ////////////////////////////////////// Started to make a "Getting everyone's updates" route, did not finish
  socket.on("updates", function(msg) {
    let listeProjects = "Mise a jour des projets , Liste des Projets : \n";
    msg.forEach(element => {
      listeProjects =
        listeProjects + JSON.stringify(element.nameService) + "\n";
    });
    alert(listeProjects);
  });
});
