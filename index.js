////////////////////////////////////// Declaring all of the needed requires
const express = require("express");
const app = express();
const path = require("path");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const http = require("http").createServer(app);
const io = require("socket.io")(http);

////////////////////////////////////// Setting the use of bodyparser
app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true
  })
);

////////////////////////////////////// DATABASE
mongoose.connect("mongodb://127.0.0.1:27017/gantt", {
  useNewUrlParser: true,
  useFindAndModify: false,
  useUnifiedTopology: true
});

const db = mongoose.connection;

db.on("error", console.error.bind(console, "Connection Error"));
db.once("open", function() {
  console.log("Connected to the database");
});

////////////////////////////////////// Route of the directory to use

app.use(express.static(path.join(__dirname, "/")));

////////////////////////////////////// Creating the Schema for our database
const ganttobjetschema = mongoose.Schema({
  id: Number,
  text: String,
  start_date: String,
  duration: Number,
  progress: Number,
  parent: Number
});

const gantt = mongoose.model("Gants", ganttobjetschema);

////////////////////////////////////// Main Server Connection
const socket = require("socket.io-client");
let client = socket.connect("http://51.15.137.122:18000/", { reconnect: true });

/////////////////////////////////////////// Connecting to the Global server
client.on("connect", () => {
  console.log("connected");

  /////////////////////////////////////////// Emiting the "needHelp" service to get Help about the global server
  client.emit("needHelp");

  ////////////////////////////////////////// Getting the Results from the "needHelp" service and displaynig them in the console
  // client.on("info", data => {
  //   console.log(data);
  // });

  ///////////////////////////////////////////  Service to get the Updates and the Projects of the other Students
  client.on("projectUpdated", data => {
    console.log(data);
    io.emit("updates", data);
  });
});

////////////////////////////////////// Load all

app.get("/load", function(req, res) {
  console.log("Connected to the front");

  const ganttdatafinal = { data: [] };
  let i = 1;

  ////////////////////////////////////// Finding all of the gantts in the database
  gantt.find(function(err, gantts) {
    if (err) {
      res.send(err);
    }
    ///////////////////////////////////// For each Gantt, we add them to a final objet to then send it to the front
    gantts.forEach(element => {
      ganttdatafinal.data.push({
        id: i,
        text: element.text,
        start_date: element.start_date,
        duration: element.duration,
        progress: element.progress,
        parent: element.parent
      });
      i++;
    });
    res.json(ganttdatafinal);
  });
});

////////////////////////////////////// Post route

app.post("/data/task", function(req, res) {
  let task = getTask(req.body);

  ////////////////////////////////////// Creating a new Gantt and adding the data from the front into it
  const newgantt = new gantt({
    id: task.id,
    text: task.text,
    start_date: task.start_date,
    duration: task.duration,
    parent: task.parent
  });
  console.log(newgantt._id);
  ////////////////////////////////////// Saving the new gant in the database
  newgantt.save(function(err, res) {
    console.log("erreur?");
    if (err) {
      console.log(err);
    }
  });
});

////////////////////////////////////// Update route
app.put("/data/task/:id", function(req, res) {
  task = getTask(req.body);
  let sid = req.params.id;
  console.log(sid);

  ////////////////////////////////////// Finding a gantt in the detabase from it's ID and updating it
  gantt.findOneAndUpdate(
    sid,
    {
      text: task.text,
      start_date: task.start_date,
      duration: task.duration,
      parent: task.parent
    },
    function(err, res) {
      console.log("Voila");
    }
  );
});

////////////////////////////////////// Delete Route
app.delete("/data/task/:id", function(req, res) {
  let sid = req.params.id;

  ////////////////////////////////////// Finding one with it's id and deleting it
  gantt.findOneAndRemove(sid, function(err, res) {
    console.log("Voila");
  });
});

////////////////////////////////////// GetTask Function
function getTask(data) {
  return {
    text: data.text,
    start_date: data.start_date,
    duration: data.duration,
    progress: data.progress || 0,
    parent: data.parent
  };
}
http.listen(3000);
